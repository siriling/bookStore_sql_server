# bookStore_sql_server

#### 介绍
JavaWeb入门项目：凌尚书城，是一个简易的图书售卖网站。

#### 软件架构

前端：jsp网页+jQuery+EL+jstl+Ajax

后端：SqlServer+JDBC+Tomcat


#### 安装教程

##### 环境准备：

jdk1.8，idea，Tomcat9，SqlServer2019

##### 项目准备：

1. 打开idea创建一个项目名为：**bookStore_sql_server**的JavaWeb工程并配置好Tomcat
2. 使用git clone命令把项目下载到本地,并覆盖到在idea中创建的项目
3. 把项目目录下的.sql文件打开，在sqlserver建立所需要的表

##### 启动前配置：

1. 按照自己的数据库信息配置src/jdbc.properties

2. 打开web/WEB-INF/lib目录：

   把kaptcha-2.3.2.jar导入(add as Library...)为kaptcha

   把taglibs-standard-impl-1.2.5.jar和taglibs-standard-spec-1.2.5.jar导入为jstl

   把servlet-api.jar导入为servlet

   把mysql的JDBC包复制到lib目录下，然后把mssql-jdbc-9.2.1.jre8.jar(文件名不一定为此名)、commons-dbutils-1.6.jar和druid-1.1.10.jar导入为sqlserver+druid

#### 使用说明

1.  项目都配置好以后点击idea中的启动
2.  书城里可注册用户
3.  书城里可增删改查图书
4.  书城里可购买图书和发货

#### 参与贡献

本项目由我一人完成，若转载请注明出处https://gitee.com/siriling/bookStore_sql_server