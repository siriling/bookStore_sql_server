--创建一个数据库
create database bookstore

--删除数据库
--drop database bookstore

--创建bs_user表
create table bs_user
(id int identity(1,1) primary key,
 username nvarchar(20),
password varchar(20),
email varchar(20)
)
--创建bs_book表
create table bs_book
(id int identity(1,1) primary key,
 title nvarchar(20),
author nvarchar(20),
price money,
sales int(11),
stock int(11),
imgPath varchar(50)
)

--创建bs_order表
create table bs_order
(id varchar(20) primary key,
createTime varchar(20),
totalCount int(11),
totalAmount money,
status int(11),
userId int(11)
)
--创建bs_orderItem表
create table bs_orderItem
(id int identity(1,1) primary key,
createTime varchar(20),
title nvarchar(20),
author nvarchar(20),
price money,
imgPath varchar(30),
count int(11),
amount money,
orderId varchar(20)
)


--删除表
drop table bs_user
drop table bs_book
drop table bs_order
drop table bs_orderItem


--查看表
select * from bs_user
select * from bs_book
select * from bs_order
select * from bs_orderItem

--先打开IDENTITY_INSERT
set IDENTITY_INSERT bs_user ON

--关闭IDENTITY_INSERT
set IDENTITY_INSERT bs_user OFF

--导入数据（bs_user）
insert into bs_user (username,password) values ('sa',123)

--设置了自增长无法修改id（待解决）
--insert into bs_user values (1000,'root',123,'1042585959@qq.com')

--导入数据（bs_user）
insert into bs_book values ('三国演义','罗贯中',50.23,300,50,'static/img/book/sgyy.jpg')
insert into bs_book values ('西游记','吴承恩',49.76,300,50,'static/img/book/xyj.jpg')
insert into bs_book values ('水浒传','施耐庵',52.34,300,50,'static/img/book/shz.jpg')
insert into bs_book values ('红楼梦','曹雪芹',50.55,300,50,'static/img/book/hlm.jpg')
insert into bs_book values ('时间简史','霍金',61.21,300,50,'static/img/book/sjjs.jpg')
insert into bs_book values ('斗罗大陆','唐家三少',32.77,2500,50,'static/img/book/dldl.jpg')

--分页（row_number()）
select top 3 * 
from (select row_number() 
over(order by id asc) as rownumber,* 
from bs_book) temp_row
where rownumber>3;

--分页（offer/fetch next）
select * from bs_book 
order by id 
offset 3 rows
fetch next 3 rows only;

--根据价格区间分页（offer/fetch next）
select * from bs_book where price between 50 and 60 order by id offset 3 rows fetch next 3 rows only;