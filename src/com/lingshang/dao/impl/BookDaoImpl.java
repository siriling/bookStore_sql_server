package com.lingshang.dao.impl;

import com.lingshang.bean.Book;
import com.lingshang.bean.Page;
import com.lingshang.dao.BaseDao;
import com.lingshang.dao.BookDao;

import java.util.List;

/**
 * Date:2020/10/11
 * Author:Ling
 * Description:图书实现类
 */
public class BookDaoImpl extends BaseDao implements BookDao {
    @Override
    public List<Book> getPageByPrice(Page<Book> page, Integer minPrice, Integer maxPrice) {
        String sql="select id,title,author,price,sales,stock,imgPath from bs_book where price between ? and ? order by id offset ? rows fetch next ? rows only";
        return getBeanList(Book.class,sql,minPrice,maxPrice,(page.getPageNo()-1)*page.getPageSize(),page.getPageSize());
    }

    @Override
    public Integer getCountByPrice(Integer minPrice, Integer maxPrice) {
        String sql="select count(id) from bs_book where price between ? and ?";
        return (Integer) getSingleData(sql,minPrice,maxPrice);
    }

    @Override
    public List<Book> getPage(Page<Book> page) {
        String sql="select id,title,author,price,sales,stock,imgPath from bs_book order by id offset ? rows fetch next ? rows only";
        return getBeanList(Book.class,sql,(page.getPageNo()-1)*page.getPageSize(),page.getPageSize());
    }

//    @Override
//    public List<Book> getBookPage(Integer pageNo, Integer pageSize) {
//        String sql="select id,title,author,price,sales,stock,imgPath from bs_book order by id offset ? rows fetch next ? rows only";
//        return getBeanList(Book.class,sql,(pageNo-1)*pageSize,pageSize);
//    }

    @Override
    public Integer getBookCount() {
        String sql="select count(id) from bs_book";
        return (Integer) getSingleData(sql);
    }

    @Override
    public List<Book> getBookList() {
        String sql="select id,title,author,price,sales,stock,imgPath from bs_book";
        return getBeanList(Book.class,sql);
    }

    @Override
    public void addBook(Book book) {
        String sql="insert into bs_book values(?,?,?,?,?,?)";
        update(sql,book.getTitle(),book.getAuthor(),book.getPrice(),book.getSales(),book.getStock(),book.getImgPath());
    }

    @Override
    public void deleteBook(String id) {
        String sql="delete from bs_book where id=?";
        update(sql,id);
    }

    @Override
    public Book getBookByBookId(String id) {
        String sql="select id,title,author,price,sales,stock,imgPath from bs_book where id=?";
        return getBean(Book.class,sql,id);
    }

    @Override
    public void updateBook(Book book) {
        String sql="update bs_book set title=?,author=?,price=?,sales=?,stock=?,imgPath=? where id=?";
        update(sql,book.getTitle(),book.getAuthor(),book.getPrice(),book.getSales(),book.getStock(),book.getImgPath(),book.getId());
    }

    @Override
    public void updateSalesAndStock(Integer bookId, Integer count) {
        String sql="update bs_book set sales=sales+?,stock=stock-? where id =?";
        update(sql,count,count,bookId);
    }
}
