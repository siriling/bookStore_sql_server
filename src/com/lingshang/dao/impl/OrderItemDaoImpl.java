package com.lingshang.dao.impl;

import com.lingshang.bean.Order;
import com.lingshang.bean.OrderItem;
import com.lingshang.dao.BaseDao;
import com.lingshang.dao.OrderItemDao;

import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderItemDaoImpl extends BaseDao implements OrderItemDao {
    @Override
    public void saveOrderItem(OrderItem orderItem) {
        String sql="insert into bs_orderItem values(?,?,?,?,?,?,?,?)";
        update(sql,orderItem.getcreateTime(),orderItem.getTitle(),orderItem.getAuthor(),orderItem.getPrice(),orderItem.getImgPath(),orderItem.getCount(),orderItem.getAmount(),orderItem.getOrderId());

    }

    @Override
    public List<OrderItem> getOrderDetails(String orderId) {
        String sql="select * from bs_orderItem where orderId=?";
        return getBeanList(OrderItem.class,sql,orderId);
    }
}
