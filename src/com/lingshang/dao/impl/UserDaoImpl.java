package com.lingshang.dao.impl;

import com.lingshang.bean.User;
import com.lingshang.dao.BaseDao;
import com.lingshang.dao.UserDao;

/**
 * Date:2020/10/4
 * Author:Ling
 * Description:dao实现类
 */
public class UserDaoImpl extends BaseDao implements UserDao {
    @Override
    public User login(String username, String password) {
        String sql="select id,username,password,email from bs_user where username=? and password=? ";
        return getBean(User.class,sql,username,password);
    }

    @Override
    public User checkUsername(String username) {
        String sql="select id,username,password,email from bs_user where username=?";
        return getBean(User.class,sql,username);
    }

    @Override
    public void registUser(User user) {
        String sql="insert into bs_user values(?,?,?)";
        update(sql,user.getUsername(),user.getPassword(),user.getEmail());
    }
}
