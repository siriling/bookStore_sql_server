package com.lingshang.servlet;

import com.lingshang.bean.Order;
import com.lingshang.bean.OrderItem;
import com.lingshang.service.OrderService;
import com.lingshang.service.impl.OrderServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderManagerServlet extends BaseServlet {

    private OrderService orderService=new OrderServiceImpl();

    //订单管理（查看所有订单）
    protected void getAllOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用service处理
        List<Order> list = orderService.getAllOrder();
        //将所有的订单信息放在请求域中
        request.setAttribute("list",list);
        //转发到order_manager.jsp
        request.getRequestDispatcher("/pages/manager/order_manager.jsp").forward(request,response);

    }

    //发货
    protected void sendOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        orderService.sendOrder(orderId);
        //重定向到来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

    //查看详情
    protected void getOrderDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        List<OrderItem> list = orderService.getOrderDetails(orderId);
        //将订单详情放在请求域中
        request.setAttribute("list",list);
        //转发跳转到订单详情页面
        request.getRequestDispatcher("/pages/manager/order_details.jsp").forward(request,response);

    }
}
