<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>订单管理</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
</head>
<body>
	
	<div id="header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
			<span class="wel_word">订单管理系统</span>
			<jsp:include page="/WEB-INF/include/manager_head.jsp"></jsp:include>
	</div>
	
	<div id="main">

		<c:if test="${not empty requestScope.order.list}">
		<table>
			<tr>
				<td>日期</td>
				<td>金额</td>
				<td>详情</td>
				<td>发货</td>
				
			</tr>		

			<c:forEach items="${requestScope.list}" var="order">
				<tr>
					<td style="width: 300px">${pageScope.order.createTime}</td>
					<td>${pageScope.order.totalAmount}</td>
					<td>
						<c:if test="${pageScope.order.status==0}">
							<a href="OrderManagerServlet?method=sendOrder&orderId=${pageScope.order.id}">点击发货</a>
						</c:if>
						<c:if test="${pageScope.order.status==1}">等待收货</c:if>
						<c:if test="${pageScope.order.status==2}">交易完成</c:if>
					</td>
					<td><a href="OrderManagerServlet?method=getOrderDetails&orderId=${pageScope.order.id}">查看详情</a></td>
				</tr>
			</c:forEach>
		</table>

		</c:if>

		<c:if test="${empty requestScope.order.list}">
			<br><br><br><br><br><br><br><br><br><br>
			<center>
				<h2>无订单</h2>
			</center>
		</c:if>
	</div>
	
	<div id="bottom">
		<span>
			宸星书城.Copyright &copy;2020
		</span>
	</div>
</body>
</html>